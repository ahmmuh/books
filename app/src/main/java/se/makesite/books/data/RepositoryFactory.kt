package se.makesite.books.data

import se.makesite.books.data.book.BookRepository

class RepositoryFactory {



    companion object {

        private var sBookRepository: BookRepository? = null

        @JvmStatic
        @Synchronized
        fun getBookRepository(): BookRepository = sBookRepository
            ?: BookRepository().also { sBookRepository = it }
    }

}
