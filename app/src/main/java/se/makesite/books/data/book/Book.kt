package se.makesite.books.data.book

data class Book (

    val name: String,
    val photo_url: String,
    val price: Double
)
