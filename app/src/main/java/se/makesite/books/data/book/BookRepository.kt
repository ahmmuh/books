package se.makesite.books.data.book

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.google.gson.reflect.TypeToken
import com.rakangsoftware.tiny.Tiny
import com.rakangsoftware.tiny.TinyResult

class BookRepository {


    private val bookListType = object : TypeToken<List<Book>>() {}.type

    fun get(): LiveData<List<Book>> {
        val result = MutableLiveData<List<Book>>()
        Tiny.fetch<Any>("https://finepointmobile.com/data/products.json", bookListType)
            .get(object : TinyResult<List<Book>> {
                override fun onSuccess(post: List<Book>) {
                    result.postValue(post)
                }

                override fun onFail(throwable: Throwable) {
                    result.postValue(arrayListOf())
                }
            })
        return result
    }
}