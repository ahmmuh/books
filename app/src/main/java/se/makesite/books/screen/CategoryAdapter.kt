package se.makesite.books.screen

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.books_activity.view.*
import se.makesite.books.R

class CategoryAdapter(val categories: List<String>): RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryAdapter.ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.books_activity, parent,false)

        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return  categories.size


    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.categoryName.text = categories[position]

    }
    class ViewHolder (itemView: View): RecyclerView.ViewHolder(itemView){

        val categoryName : TextView = itemView.categoryName

    }

}