package se.makesite.books.screen.books

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import se.makesite.books.R

class BooksActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)


        val viewModel = ViewModelProviders.of(this).get(BooksViewModel::class.java)
        viewModel.books.observe(this, Observer { it ->
            it?.let { books ->
                d("Ahmed", "hämtar böcker ${books.size} st")
            }
        })
    }



    companion object {
        fun start(context: Context){

            context.startActivity(Intent(context, BooksActivity::class.java))

        }
    }
}
