package se.makesite.books.screen.books

import android.arch.lifecycle.ViewModel
import se.makesite.books.data.RepositoryFactory

class BooksViewModel: ViewModel() {

    val books = RepositoryFactory.getBookRepository().get()
}