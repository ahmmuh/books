package se.makesite.books.screen.splash

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import se.makesite.books.screen.books.BooksActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        BooksActivity.start(this)
        finish()
    }
}
